package org.handlersolutions;

import java.io.*;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.cert.*;
import java.util.Base64;

import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.ocsp.OCSPObjectIdentifiers;
import org.bouncycastle.asn1.x509.Extension;
import org.bouncycastle.asn1.x509.Extensions;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.ocsp.*;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.bc.BcDigestCalculatorProvider;

/**
 * Notas extras:
 * Para saber cual es el padre de un certificado es necesario tener los certificados raíz vigentes en este caso del SAT
 * <a href="http://omawww.sat.gob.mx/tramitesyservicios/Paginas/certificado_sello_digital.htm">Sitio del SAT</a>
 * Iterar cada certificado raíz y parsearlo en un objeto X509Certificate
 * Se ejecuta el metodo verify del customerCertificate y en el parametro se pasa la llave publica del certificado raíz obtenida con el metodo getPublicKey()
 * Si no lanza una excepción es porque el certificado si es el padre.
 * <p>
 * En este caso no se implementa para no hacer mas largo y complejo el ejemplo.
 * Aquí el certificado padre ya comprobado anteriormente es el AC5_SAT.cer
 */
public class Main {
    public static void main(String[] args) throws Exception {
        // Certificado a verificar
        byte[] certificate = Base64.getDecoder().decode("MIIGSjCCBDKgAwIBAgIUMDAwMDEwMDAwMDA1MDMyNDUyMDAwDQYJKoZIhvcNAQELBQAwggGEMSAwHgYDVQQDDBdBVVRPUklEQUQgQ0VSVElGSUNBRE9SQTEuMCwGA1UECgwlU0VSVklDSU8gREUgQURNSU5JU1RSQUNJT04gVFJJQlVUQVJJQTEaMBgGA1UECwwRU0FULUlFUyBBdXRob3JpdHkxKjAoBgkqhkiG9w0BCQEWG2NvbnRhY3RvLnRlY25pY29Ac2F0LmdvYi5teDEmMCQGA1UECQwdQVYuIEhJREFMR08gNzcsIENPTC4gR1VFUlJFUk8xDjAMBgNVBBEMBTA2MzAwMQswCQYDVQQGEwJNWDEZMBcGA1UECAwQQ0lVREFEIERFIE1FWElDTzETMBEGA1UEBwwKQ1VBVUhURU1PQzEVMBMGA1UELRMMU0FUOTcwNzAxTk4zMVwwWgYJKoZIhvcNAQkCE01yZXNwb25zYWJsZTogQURNSU5JU1RSQUNJT04gQ0VOVFJBTCBERSBTRVJWSUNJT1MgVFJJQlVUQVJJT1MgQUwgQ09OVFJJQlVZRU5URTAeFw0yMDAyMjAxOTE1MDhaFw0yNDAyMjAxOTE1NDhaMIHmMScwJQYDVQQDEx5KQUlSIEVSTkVTVE8gR09OWkFMRVogQk9OQVNUUkUxJzAlBgNVBCkTHkpBSVIgRVJORVNUTyBHT05aQUxFWiBCT05BU1RSRTEnMCUGA1UEChMeSkFJUiBFUk5FU1RPIEdPTlpBTEVaIEJPTkFTVFJFMQswCQYDVQQGEwJNWDEnMCUGCSqGSIb3DQEJARYYamFpcmdvbnphbGV6NjBAZ21haWwuY29tMRYwFAYDVQQtEw1HT0JKOTYwNTE1RTg2MRswGQYDVQQFExJHT0JKOTYwNTE1SFZaTk5SMDUwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCT6+BB121FIj2Ng0oOxiPwKEHQFoAbSHjQHoRmbUaZCyLD9mY+VvxD5aixo+SBa2i5DcA7Qgwb1PKm2pZk67yJWvD9T32/eR1lEnsa2jPCcYWcKrmIYH8roF77xhw+F7VL8lOji9gPtZIAzKioCyDigeyWJVNYz0vq6TUPrNDl1RWyt+M0GybYXNIzzPxAi8lxbtHu2s7kaRP7TWHSU6hpRFRKjd3msMZslqunEpllMly/5QgZwkJcggLGjN98xqeaK3cuJZ5BLkpTOtl/8wko8JeFsfBqU1zYH8vaGslO3bZSVLDn2JjwMAOBexhfixb+G5wy4g26W1dwpAKbPUglAgMBAAGjTzBNMAwGA1UdEwEB/wQCMAAwCwYDVR0PBAQDAgPYMBEGCWCGSAGG+EIBAQQEAwIFoDAdBgNVHSUEFjAUBggrBgEFBQcDBAYIKwYBBQUHAwIwDQYJKoZIhvcNAQELBQADggIBALuxrxD2hNHPTen5fkFPhjxnL5VRNNLKxxO1N6GknV+gJNvEit7rXkFc3uYXetQ79xuEAWOx642uEK6K6q9AB/cXQitHvRU/TZ935oA0qv7Ru/MkOuqZmDtIxO5LpDJ4R6Fqyx7+LC2E+4UNJYK3TSWdJIXOcMGxlk5f4aPXh0m+Wg1/R1Z6Br+HWT6dLivqNMca9c8dJel155R8vpZMFUBhhsT0qxnRNAYr4Hcj4wGSHImyiY+SsS/dZRdt1mb1HQmo/2p3YQGODj5uZub0jJk2xelL0o1JXkh6tWmBS9x8yBqGactVheKekWT3zsxcq3O5drGL4LFtOCeOM+Qpgh/w6m7GRDhlFeFcjRHCX+rfCPBGAKBVTx3LXcgyTJ2jiF1pd7NczU/w6jvbeJp+IhiBY5j4FHw2gue7AnCALY9BdeqyMXfkuWjIwc1Pg+Dn4I2iY4bUM9vnweR5T+But/wPQS3Fkl7OcE9xUcl1TPRKbVpwkyoI5hGrzc1CWLCJDFvxRZZGibc65+IcuWwcNadjTgVc0E3M8h//ZO+iVOFHrHeL1GlyC9BKP6prGkxA7E5Tij7ema+4rjjxdO9v5OZxeDQVGsd2AUDimBPCOzqW7umnbvTH2yg9iUMYEOQ7eZUWKHTLpx4IpGh5+OBwXLy/2jX3uXUR/x3HQX6o19/M");
        // Certificado raíz (este certificado debe ser padre del certificado a verificar)
        byte[] caCertificate = Base64.getDecoder().decode("MIIIyjCCBrKgAwIBAgIUMDAwMDAwMDAwMDAwMDAwMDExMDYwDQYJKoZIhvcNAQELBQAwggERMQswCQYDVQQGEwJNWDENMAsGA1UECBMEQ0RNWDETMBEGA1UEBxMKQ1VBVUhURU1PQzEYMBYGA1UEChMPQkFOQ08gREUgTUVYSUNPMQ0wCwYDVQQLEwRHVFNQMSUwIwYDVQQDExxBR0VOQ0lBIFJFR0lTVFJBRE9SQSBDRU5UUkFMMRIwEAYGdYhdjzUfEwZGMDk2NjUxETAPBgZ1iF2PNRETBTA2MDAwMRcwFQYGdYhdjzUTEws1IERFIE1BWU8gMjEoMCYGCSqGSIb3DQEJAhMZSlVBTiBBTlRPTklPIFJPQ0hBIFZBTERFWjEkMCIGCSqGSIb3DQEJARYVYXJAaWVzLmJhbnhpY28ub3JnLm14MB4XDTE5MDUwMzE2MTkwOVoXDTI3MDUwMzE2MTkwOVowggGEMSAwHgYDVQQDDBdBVVRPUklEQUQgQ0VSVElGSUNBRE9SQTEuMCwGA1UECgwlU0VSVklDSU8gREUgQURNSU5JU1RSQUNJT04gVFJJQlVUQVJJQTEaMBgGA1UECwwRU0FULUlFUyBBdXRob3JpdHkxKjAoBgkqhkiG9w0BCQEWG2NvbnRhY3RvLnRlY25pY29Ac2F0LmdvYi5teDEmMCQGA1UECQwdQVYuIEhJREFMR08gNzcsIENPTC4gR1VFUlJFUk8xDjAMBgNVBBEMBTA2MzAwMQswCQYDVQQGEwJNWDEZMBcGA1UECAwQQ0lVREFEIERFIE1FWElDTzETMBEGA1UEBwwKQ1VBVUhURU1PQzEVMBMGA1UELRMMU0FUOTcwNzAxTk4zMVwwWgYJKoZIhvcNAQkCE01yZXNwb25zYWJsZTogQURNSU5JU1RSQUNJT04gQ0VOVFJBTCBERSBTRVJWSUNJT1MgVFJJQlVUQVJJT1MgQUwgQ09OVFJJQlVZRU5URTCCAiIwDQYJKoZIhvcNAQEBBQADggIPADCCAgoCggIBAMDANv2roMMqVQUpFeKNGo1SGJRsp5xEjO/ZjCV80fdE/FQLP3eWnpqJ0PiAKa4zDQz8E5EK2zcym/NSMR1MWRPjN5d4ZHh4Ysu4irIj4t1T1DQygRtQP8KMofNNIS4XaP8LA/2bz33qhhb0o40VSmX7LgNcdbZZwYBPc4tkO+VoEPvnn8SLycFACmgb28xd7nMZPmWolIY11Zykf3gMY8rvXVYfNrMQpJHJNjMH5KhLluhiV4JC9xcgqhaO2rJX43D3Ej8Z5SDWI3CVqdt8RwRMfZaGCN8JqjNynm0aoeIoQmleowKqQv4TClsqgyBoG17T7okFIsYC7eHSVR3/ACbWEEUzXNsViz7HrfCzlEDqA6YeaW1ZMbW+O42QIDJx8/3SuCSQ19upXvkJuVcg7mRNqWDRkWkyayWRYVUUl2IpRGCgrmZPjc0C2y3Xx5MHCboCV/qshYS7JPM3Grh8rUh9QotKaGBXjs2Hlmcm+EzcrHgwa4DEDGszano0cKKrK7sGEJ8/AXM88VpZARlzxTVY0NgFB9ExrSr5oTlLt72miFD+wfuc+mEQIghnC+G5BJReiMr03hqQG0GLSsAHZ89r0KjlE80491CEYczGuC7RgmbHinmhq54IbuWcihaAQghgmU+b+dbhz21F9HYkCS2M0CxDxu6gyr5qCMgG+U2lAgMBAAGjggGhMIIBnTCCAVYGA1UdIwSCAU0wggFJgBRvsJeWPKAOVydU9sCSNoR7gNJBaaGCARmkggEVMIIBETELMAkGA1UEBhMCTVgxDTALBgNVBAgTBENETVgxEzARBgNVBAcTCkNVQVVIVEVNT0MxGDAWBgNVBAoTD0JBTkNPIERFIE1FWElDTzENMAsGA1UECxMER1RTUDElMCMGA1UEAxMcQUdFTkNJQSBSRUdJU1RSQURPUkEgQ0VOVFJBTDESMBAGBnWIXY81HxMGRjA5NjY1MREwDwYGdYhdjzUREwUwNjAwMDEXMBUGBnWIXY81ExMLNSBERSBNQVlPIDIxKDAmBgkqhkiG9w0BCQITGUpVQU4gQU5UT05JTyBST0NIQSBWQUxERVoxJDAiBgkqhkiG9w0BCQEWFWFyQGllcy5iYW54aWNvLm9yZy5teIIUMDAwMDAwMDAwMDAwMDAwMDAwMDQwHQYDVR0OBBYEFN9p3Al26CZQ32833t4UrLLnkCkjMBIGA1UdEwEB/wQIMAYBAf8CAQAwDgYDVR0PAQH/BAQDAgH2MA0GCSqGSIb3DQEBCwUAA4ICAQBwZvnsi0JrD5WtPkHDbBdXM+IpqnuExg+tg2AxmtHRzHXGRjDXGElHYRaQJ3qzTNTCE2T/cBVodWthhhSivkPBhtJd/EVmz2bb1QEOt9Ah7og470AcdqJcVfawkHnqJI06KnRwp8DwEvqbv2GS9GLN+eYrKgmRe84i3ckDyaLSZKMXf+Pdd7BBkYTWFVqrNQtW4hvhPphVujoLj4Zxl1zu8r4/VrWqbIT+KdhsKiwBk2WhFHqUB6MSOjIA4nN6f2oVaAN7vNnOIk8TmWP3X+xBrx/u3u6v8+3mUf6YTP7NabEypVM55yHG57xNjz3y/ZiePaiu+Kali+JGDEy8Y9/62+OZLRBumkluZbys7qiKpkusphsAiMBHXkEOOFN1rWzJJNoIxIHvDyDZ9rhjdTd3cPmxoPVXiF5HgmUCD0VSui/NEMSQBc2sVejUoKoxBnW/XYyzR33FKZu2Zvzk3SCLiE61zpPUbo49mROXMsHMP0insgNEu/Juzi38nUllLlPEOM0XarEk/jJti+t2rYgEk6YuCbRS04PqMiuY9gUiJ+A3NdipiVKGnRp7GTFoK3kFfYvksub+1yxhH9QfHwgAmYxmROj6FhGOZi7ZsUyBPP16Pg1uXkZvH0UHxzJU6pC0kaSBcaLq6gEIyRZP6Rwtt4UmzI9EJNqIa9N7U1k2GQ==");
        CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
        // Objetos para manipular el certificado en el código
        X509Certificate customerCertificate = (X509Certificate) certificateFactory.generateCertificate(new ByteArrayInputStream(certificate));
        X509Certificate caCertificateParsed = (X509Certificate) certificateFactory.generateCertificate(new ByteArrayInputStream(caCertificate));
        BigInteger customerSerialNumber = customerCertificate.getSerialNumber();

        byte[] ocspRequest = ocspRequest(caCertificateParsed, customerSerialNumber).getEncoded();
        OCSPResp ocspResp = ocspConnection(ocspRequest);

        if (ocspResp.getResponseObject() == null) {
            throw new Exception("El certificado es inválido");
        }
        if (((BasicOCSPResp)ocspResp.getResponseObject()).getResponses()[0].getCertStatus() == CertificateStatus.GOOD) {
            System.out.println("El certificado es válido");
        } else {
            System.out.println("El certificado es inválido");
        }
        // Muestra la información del certificado
        System.out.println(customerCertificate.getSubjectDN());
    }

    /**
     * Metodo que prepara la request asn1 con el certificado raíz y el número de serie del certificado a verificar
     * Se usa la librería bouncycastle para agilizar este proceso
     * @param caCertificate Certificado raíz
     * @param customerSerialNumber  Número de serie
     * @return Regresa un objeto OCSPReq, el cual contiene el asn1 a envíar al servicio del SAT
     * @throws OperatorCreationException
     * @throws CertificateEncodingException
     * @throws IOException
     * @throws OCSPException
     */
    private static OCSPReq ocspRequest(X509Certificate caCertificate, BigInteger customerSerialNumber) throws OperatorCreationException, CertificateEncodingException, IOException, OCSPException {
        BcDigestCalculatorProvider util = new BcDigestCalculatorProvider();

        CertificateID id = new CertificateID(util.get(CertificateID.HASH_SHA1), new X509CertificateHolder(caCertificate.getEncoded()), customerSerialNumber);
        OCSPReqBuilder ocspRequestGenerated = new OCSPReqBuilder();
        ocspRequestGenerated.addRequest(id);

        BigInteger nonce = BigInteger.valueOf(System.currentTimeMillis());
        Extension[] extensions = new Extension[1];
        Extension ext = new Extension(OCSPObjectIdentifiers.id_pkix_ocsp_nonce, true, new DEROctetString(nonce.toByteArray()));
        extensions[0] = ext;
        ocspRequestGenerated.setRequestExtensions(new Extensions(extensions));

        return ocspRequestGenerated.build();
    }

    /**
     * Simple conexión al servicio OCSP del sat usando librerías nativas de Java
     * Importante los headers de la request "content-type" y "accept" para seguir la especificación del protocolo OCSP
     *
     * @param request   Request ASN1 antes construida
     * @return  OCSPResp objeto con la respuesta parseada
     * @throws Exception
     */
    private static OCSPResp ocspConnection(byte[] request) throws Exception {
        String url = "https://cfdi.sat.gob.mx/edofiel";

        HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
        connection.setRequestProperty("Content-Type", "application/ocsp-request");
        connection.setRequestProperty("Accept", "application/ocsp-response");
        connection.setDoOutput(true);

        System.out.println("Sending OCSP request to <{}>" + url);

        DataOutputStream outputStream = new DataOutputStream(new BufferedOutputStream(connection.getOutputStream()));
        outputStream.write(request);    // Se carga el request asn1 en el body de la request
        outputStream.flush();
        outputStream.close();

        if (connection.getResponseCode() != 200) {
            throw new Exception("Falló al conectarse con el servicio OCSP");
        }

        InputStream response = (InputStream) connection.getContent();
        return new OCSPResp(response);  // Se usa la librería bouncycastle para construir un objeto con la respuesta del servicio
    }
}