# Ejemplo básico para consumir el servicio OCSP del SAT

## Requisitos minimos
- Java 11
- Maven 3.8.1

## Corriendo el proyecto desde terminal
Instala las dependencias del proyecto:
```
$ cd /path/to/project
$ mvn clean install
```

Para correr localmente puedes usar:
```
$ mvn exec:java
```

## Correr en IDE
Se recomienda usar Intellij IDEA de Jetbrains y solo ejecutar la clase pricipal main

## Autores
- [Jair Bonastre](https://jairbonastre.com)
